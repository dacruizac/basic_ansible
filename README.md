# Basic Ansible example

In this repository it is shown a basic example of a deployment using ansible with centos containers in docker. In this case, a static web page will be provided by a node using an ansible manager to install the required packages in the node and starting the proper service.

Please remember that ansible can send orders to other machine using ssh, so it is necessary to establish a conection with the node machine

## Build images

In this project there are 3 images that have to be built. The first image is in the folder Base_centos, and have to be built before the image of the node.

* Base CentOS: To build the image it is necessary to go to the folder Base_centos and run:

```
docker build -t base_centos:1.0 .
```

* Ansible node: To build the image it is necessary to go to the folder ansible_node_container and run:

```
docker build -t centos7_ssh_node:1.0 .
```

* Ansible manager: To build the image it is necessary to go to the folder ansible_manager_container and run:

```
docker build -t centos7_ansible_manager:1.0 .
```

## Execute ansible node

In order to run the ansible node, it is necessary to run the ansible node, it is necessary to run the following command:

```
docker run -d --privileged=true -p 8080:80 centos7_ssh_node:1.0
```

and to enable the ssh connections, then it is important to run:

```
docker exec ansible_node_1 /start.sh
```

## Execute manager

In order to run the manager container and access the manager console, there is important to run:

```
docker run --privileged --expose 22 -it centos:centos7
```

## Execute ansible

### Create ssh key and connect with the node:

There are 2 commands to run:

```sh
ssh-keygen
```

```sh
ssh-copy-id 172.17.0.2
```

### Execute the playbook

To finally execute the playbook, then run:

```
ansible-playbook -i inventory web.yaml
```